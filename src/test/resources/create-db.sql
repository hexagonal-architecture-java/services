create table account (
  number integer primary key,
  bank_balance decimal (10,2),
  account_holder varchar(200)
);