package integration;

import account.system.domain.model.BusinessException;
import account.system.port.AccountRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.inject.Inject;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Account - database service")
@ContextConfiguration(classes = Config.class)
@ExtendWith(SpringExtension.class)
public class AccountRepositoryTest {

    @Inject
    AccountRepository accountRepository;

    @Test
    @DisplayName("get account with null value")
    void test1() {
        try {
            var account = accountRepository.get(null);
            assertNull(account, "Account must be null");
        } catch (BusinessException e) {
            fail("Must be load a null account");
        }
    }

    @Test
    @DisplayName("account research with a nonexistent number")
    void test2() {
        try {
            var account = accountRepository.get(8547);
            assertNull(account, "Account should be null");
        } catch (BusinessException e) {
            fail("Should load a null account");
        }
    }

    @Test
    @DisplayName("account research with an existent number")
    void test3() {
        try {
            var account = accountRepository.get(50);
            assertNotNull(account, "Account should not be null");
            System.out.println(account);
        } catch (BusinessException e) {
            fail("Should load an existing account");
        }
    }

    @Test
    @DisplayName("update account with null value")
    void test4() {
        try {
            accountRepository.update(null);
            fail("Should not update a null account");
        } catch (BusinessException e) {
            assertEquals(e.getMessage(), "Account is required.");
            System.out.println(e.getMessage());
        }
    }

    @Test
    @DisplayName("update account successfully")
    void test5() {
        try {
            var account = accountRepository.get(50);
            account.setBankBalance(new BigDecimal("1.00"));
            account.setAccountHolder("New Account Holder");
            accountRepository.update(account);

            var account2 = accountRepository.get(50);
            assertEquals(account.getBankBalance(), account2.getBankBalance(), "Should bank balance is equal");
            assertEquals(account.getAccountHolder(), account2.getAccountHolder(), "Should account holder is equal");
        } catch (BusinessException e) {
            fail("Should update account");
        }
    }


}
