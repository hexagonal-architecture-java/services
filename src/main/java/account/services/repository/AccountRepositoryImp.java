package account.services.repository;

import account.system.domain.model.Account;
import account.system.domain.model.BusinessException;
import account.system.port.AccountRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import static java.util.Objects.isNull;

@Named
public class AccountRepositoryImp implements AccountRepository {

    private static final String ERROR = "An error occurred while accessing the database";

    private final JdbcTemplate jdbc;

    @Inject
    public AccountRepositoryImp(JdbcTemplate jdbcTemplate) {
        this.jdbc = jdbcTemplate;
    }

    @Override
    public Account get(Integer number) {
        if (isNull(number)) {
            return null;
        }

        var sqlSelectAccount = "SELECT * FROM account WHERE number=?";
        var param = new Object[]{number};
        RowMapper<Account> orm = (resultSet,  control) ->
                new Account(resultSet.getInt(1), resultSet.getBigDecimal(2), resultSet.getString(3));

        try {
            var list = this.jdbc.query(sqlSelectAccount, param, orm);

            if (list.isEmpty()) {
                return null;
            }
            //return the first array position
            return list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ERROR);
        }
    }

    @Transactional
    @Override
    public void update(Account account) {
        if (isNull(account)) {
            throw new BusinessException("Account is required.");
        }

        var sqlUpdateAccount = "UPDATE account SET bank_balance=?, account_holder=? WHERE number=?";
        var param = new Object[]{account.getBankBalance(), account.getAccountHolder(), account.getNumber()};

        try {
            this.jdbc.update(sqlUpdateAccount, param);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ERROR);
        }
    }
}
