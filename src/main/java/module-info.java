module account.services {
    // use system
    requires account.system;

    // use spring
    requires javax.inject;
    requires spring.tx;
//    requires spring.core;
//    requires spring.beans;
//    requires spring.context;
    requires java.sql;
    requires spring.jdbc;

    //open repository
    opens account.services.repository;
}